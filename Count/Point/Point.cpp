#include "Point.h"
#include <Windows.h>
#include <iostream>


Point::Point()
{
}

void Point::SetPosition(int x,int y)
{
	_x = x;
	_y = y;
	GotoXY(_x, _y);
}

void Point::Move()
{
	std::cin >> _x >> _y;
	GotoXY(_x, _y);
}

void Point::GotoXY(int x, int y)
{
	COORD pos = { x,y };
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);

	std::cout << '*' << std::endl;

}

Point::~Point()
{
}
