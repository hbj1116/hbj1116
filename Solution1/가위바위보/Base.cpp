#include "pch.h"

Base::Base()
{
}

void  Base::PlayGame()
{
	cout << "Rock Scissors Paper (R/S/P)";
	cin >> RockScissorsPaper;

	if (RockScissorsPaper == 'r' || RockScissorsPaper == 'R')
	{
		cout << "Rock" << endl;
	}
	else if (RockScissorsPaper == 'S' || RockScissorsPaper == 's')
	{
		cout << "Scissors" << endl;
	}
	else if (RockScissorsPaper == 'p' || RockScissorsPaper == 'P')
	{
		cout << "Paper" << endl;
	}
}

bool Base::Endgame()
{
	if (RockScissorsPaper == 'Q' || RockScissorsPaper == 'q')
	{
		return true;
	}

	return false;
}

char Base::Battle(char _RockScissorsPaper)
{
	if (RockScissorsPaper == 'R' || RockScissorsPaper == 'r')
	{
		if (_RockScissorsPaper == 'R' || _RockScissorsPaper == 'r')
		{
			cout << "Draw" << endl;
			return 'D';
		}
		else if (_RockScissorsPaper == 'S' || _RockScissorsPaper == 's')
		{
			cout << "Win" << endl;
			UpWinCount();
			return 'W';
		}
		else if (_RockScissorsPaper == 'P' || _RockScissorsPaper == 'p')
		{
			cout << "Lose" << endl;
			return 'L';
		}
	}
	else if (RockScissorsPaper == 'S' || RockScissorsPaper == 's')
	{
		if (_RockScissorsPaper == 'R' || _RockScissorsPaper == 'r')
		{
			cout << "Lose" << endl;
			return 'L';
		}
		else if (_RockScissorsPaper == 'S' || _RockScissorsPaper == 's')
		{
			cout << "Draw" << endl;
			return 'D';
		}
		else if (_RockScissorsPaper == 'P' || _RockScissorsPaper == 'p')
		{
			cout << "Win" << endl;
			UpWinCount();
			return 'W';
		}
	}
	else if (RockScissorsPaper == 'P' || RockScissorsPaper == 'p')
	{
		if (_RockScissorsPaper == 'R' || _RockScissorsPaper == 'r')
		{
			cout << "Win" << endl;
			UpWinCount();
			return 'W';
		}
		else if (_RockScissorsPaper == 'S' || _RockScissorsPaper == 's')
		{
			cout << "Lose" << endl;
			return 'L';
		}
		else if (_RockScissorsPaper == 'P' || _RockScissorsPaper == 'p')
		{
			cout << "Draw" << endl;
			return 'D';
		}
	}
}

void Base::UpWinCount()
{
	++WinCount;
}

int Base::PrintWinCount()
{
	cout << "You Win : " << WinCount << endl;
	return WinCount;
}

Base::~Base()
{
}
