#pragma once
class point
{
public:
	point();
	point(int x, int y);
	void show();

	point operator++();
	point operator++(int num);

	~point();
; private:
	int _x, _y;
};

