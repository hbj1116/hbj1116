#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif _MSC_VER
#include <iostream>

int OpenAndCloseFileStream();
int MultipliCationTable(FILE* fp);

int main()
{
	OpenAndCloseFileStream();
}


int OpenAndCloseFileStream()
{
	FILE* fp;

	if ((fp = fopen("test.txt", "wt")) == NULL)
	{
		printf("파일 열기에 실패하였습니다.\n");
		return -1;
	}

	MultipliCationTable(fp);

	fclose(fp);

	system("pause");

	return 0;
}

int MultipliCationTable(FILE* fp)
{
	int InputValue = 0;
	std::cin >> InputValue;
	
	fprintf(fp,"%d단\n", InputValue);
	for (int i = 1; i < 10; ++i)
	{
		fprintf(fp,"%d * %d = %d\n", InputValue, i, InputValue*i);
	}

	return 0;
}
