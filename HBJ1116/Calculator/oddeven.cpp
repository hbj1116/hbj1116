#include <iostream>
#define INPUT_SIZE 10

void InputData(int* temp, int* even, int* odd, int evenIndex, int oddIndex);
void OutputData(int* even, int* odd);

int main()
{
	int temp[INPUT_SIZE] = { 0 };
	int even[5] = { 0 };
	int odd[5] = { 0 };
	int evenIndex = 0;
	int oddIndex = 0;

	InputData(temp, even, odd, evenIndex, oddIndex);
	OutputData(even, odd);


	system("pause");
	
}

void InputData(int* temp, int* even, int* odd, int evenIndex, int oddIndex)
{
	for (int i = 0; i < 10; ++i)
	{
		std::cin >> temp[i];

		if (temp[i] % 2 == 0)
		{
			even[evenIndex] = temp[i];
			evenIndex++;
		}
		else if (temp[i] % 2 == 1)
		{
			odd[oddIndex] = temp[i];
			oddIndex++;
		}
	}
}

void OutputData(int* even, int* odd)
{
	std::cout << "¦�� : ";
	for (int i = 0; i < 5; ++i)
	{
		std::cout << even[i] << " ";
	}
	std::cout << std::endl;
	std::cout << "Ȧ�� : ";
	for (int i = 0; i < 5; ++i)
	{
		std::cout << odd[i] << " ";
	}
}


/*
int ar[10];
	int odd[10] = { 0 };
	int even[10] = { 0 };
	int i;
	std::cin >> ar[0] >> ar[1] >> ar[2] >> ar[3] >> ar[4] >> ar[5] >> ar[6] >> ar[7] >> ar[8] >> ar[9];

for (i = 0; i < 10; ++i)
{

	if (ar[i] % 2 == 1)
		odd[i] = ar[i];
	else
		even[i] = ar[i];
}
printf("Ȧ�� : ");
for (i = 0; i < 10; ++i)
{
	if (odd[i] % 2 == 1)
		printf("%d ", odd[i]);
}
printf("\n¦�� : ");
for (i = 0; i < 10; ++i)
{
	if (even[i] == 0)
		continue;
	else if (even[i] % 2 == 0)
		printf("%d ", even[i]);
}

*/